from __future__ import unicode_literals
from frappe import _


def get_data():
    return [
        {
            "label": _("Pictures"),
            "icon": "fa fa-star",
            "items": [
                {
                    "type": "doctype",
                    "name": "Pictures",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Picture Material",
                    "onboard": 1,
                },
            ]
        },
        {
            "label": _("Scanner"),
            "icon": "fa fa-star",
            "items": [
                {
                    "type": "doctype",
                    "name": "Picture Scan",
                    "onboard": 1,
                },
            ]
        },
        {
            "label": _("Picture Material"),
            "icon": "fa fa-star",
            "items": [
                {
                    "type": "doctype",
                    "name": "Author Of Material",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Researcher Name",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Picture Material",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Material Type",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Hisorical Era",
                    "onboard": 1,
                },
            ]
        },
        {
            "label": _("Report"),
            "icon": "fa fa-star",
            "items": [
                {
                    "type": "report",
                    "name": "Material Status",
                    "doctype": "Picture Scan",
                    "onboard": 1,
                },
            ]
        },
    ]
