from __future__ import unicode_literals
from frappe import _


def get_data():
    return [
        {
            "label": _("Map Archive Unit"),
            "icon": "fa fa-star",
            "items": [
                {
                    "type": "doctype",
                    "name": "Map Site",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Map Group",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Map Area",
                    "onboard": 1,
                },
            ]
        },
        {
            "label": _("Map Saving Place"),
            "icon": "fa fa-star",
            "items": [
                {
                    "type": "doctype",
                    "name": "Organization",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Map Owner",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Legal Status",
                    "onboard": 1,
                },
            ]
        },
        {
            "label": _("Document Creator"),
            "icon": "fa fa-star",
            "items": [
                {
                    "type": "doctype",
                    "name": "Map Creator",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Legal Foundation",
                    "onboard": 1,
                },
            ]
        },
        {
            "label": _("Maps"),
            "icon": "fa fa-star",
            "items": [
                {
                    "type": "doctype",
                    "name": "Map Sort",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Map",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Map Evaluation",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Map Restoration",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Transfer Map",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Document Type",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Document Description",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Map Saving Method",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Map Status",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Map Material Type",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Drawing Map Scale",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Technique Map Drawing",
                    "onboard": 1,
                },
            ]
        },
        {
            "label": _("Archive"),
            "icon": "fa fa-star",
            "items": [
                {
                    "type": "doctype",
                    "name": "Archive Attachment",
                    "onboard": 1,
                },
            ]
        },
        {
            "label": _("Scanner"),
            "icon": "fa fa-star",
            "items": [
                {
                    "type": "doctype",
                    "name": "Map Scanning",
                    "onboard": 1,
                },
            ]
        },
        {
            "label": _("Document Information"),
            "icon": "fa fa-star",
            "items": [
                {
                    "type": "doctype",
                    "name": "Map Creator",
                    "onboard": 1,
                },
                {
                    "type": "doctype",
                    "name": "Legal Foundation",
                    "onboard": 1,
                },
            ]
        },
        {
            "label": _("Report"),
            "icon": "fa fa-star",
            "items": [
                {
                    "type": "report",
                    "name": "Map Status",
                    "doctype": "Map Scanning",
                    "onboard": 1,
                },
            ]
        },
    ]
