# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Athar",
			"color": "pink",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Maps Archive")
		},
		{
			"module_name": "Pictures Archive",
			"color": "pink",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Pictures Archive")
		}

	]
