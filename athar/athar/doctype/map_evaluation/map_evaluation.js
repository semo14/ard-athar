// Copyright (c) 2021, AnvilErp and contributors
// For license information, please see license.txt

frappe.ui.form.on("Map Evaluation", {
  refresh: function (frm) {
    cur_frm.set_df_property("map", "read_only", cur_frm.doc.from_map == 1);

    frm.set_query("map", function () {
      return {
        filters: { docstatus: 1, evaluated: 0 },
      };
    });
  },
});
