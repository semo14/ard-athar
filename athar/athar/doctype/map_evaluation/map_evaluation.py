# -*- coding: utf-8 -*-
# Copyright (c) 2021, AnvilErp and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class MapEvaluation(Document):
	def on_submit(self):
		map = frappe.get_doc('Map', self.map)
		map.db_set('evaluated', 1, update_modified=False)
		frappe.db.commit()
	
	def on_cancel(self):
		map = frappe.get_doc('Map', self.map)
		map.db_set('evaluated', 0, update_modified=False)
		frappe.db.commit()

