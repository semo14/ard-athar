// Copyright (c) 2021, AnvilErp and contributors
// For license information, please see license.txt

frappe.ui.form.on("Map Sort", {
  refresh: (frm) => {
    if (frm.doc.docstatus == 1) {
      frm.set_df_property("number_of_maps", "read_only", 1);
      frm.add_custom_button(__("Create Map"), () => {
        if (frm.doc.number_of_maps > parseInt(frm.doc.current_number)) {
          frappe.run_serially([
            () => frappe.new_doc("Map"),
            () => frappe.timeout(1),
            () => cur_frm.set_value("roll_number", frm.doc.roll_number),
          ]);
        } else {
          frappe.msgprint("Roll Is Full");
        }
        frm.refresh_field("maps_name");
      });
      frm.add_custom_button(__("Transfer To Archive"), () => {
        // let maps = [];
        // frm.doc.maps_name.forEach((element) => {
        //   if (element.map_status != "Archived")
        //     maps.push(element.map);
        // });
        if (frm.doc.number_of_maps > 0) {
          let maps = [];
          frm.doc.maps_name.forEach((element) => {
            if (element.map_status == "Saved"){
            maps.push(element.map);
          }
          });
          let d = new frappe.ui.Dialog({
            title: "Transfer To Archive",
            fields: [
              {
                label: "Map",
                fieldname: "map",
                fieldtype: "Link",
                options:'Map',
                reqd: 1,
                get_query: function (doc) {
                  return { filters: { name: ["in", maps] } };
                },
              },
              {
                label: "Shelf Number",
                fieldname: "shelf_number",
                fieldtype: "Data",
                reqd: 1,
              },
              {
                label: "Description",
                fieldname: "description",
                fieldtype: "SmallText",
                reqd: 1,
              },
              {
                label: "Attach File",
                fieldname: "attach_file",
                fieldtype: "Attach",
                reqd: 1,
              },
              {
                label: "Transfer Date",
                fieldname: "transfer_date",
                fieldtype: "Datetime",
                reqd: 1,
              },
            ],
            primary_action_label: "Transfer",
            primary_action(values) {
              frappe.call({
                method: "add_to_archive",
                doc: frm.doc,
                args: {
                  map: values.map,
                  shelf_number: values.shelf_number,
                  description: values.description,
                  transfer_date: values.transfer_date,
                  attach_file: values.attach_file,
                },
                callback: (r) => {
                  if (!r.exc) {
                    frappe.msgprint(`Transferd To Archive Successfully ! <br>
                    <a href='/desk#Form/Archive%20Attachment/${r.message.link}'>Submit Archive Here</a>`);
                    frm.doc.maps_name.forEach((row) => {
                      if (row.map == values.map) {
                        row.map_status = "Archived"
                        row.link_to = r.message.link
                        frappe.run_serially([
                          () => frappe.db.set_value("Number Of Maps", row.name, 'map_status', "Archived"),
                          () => frappe.db.set_value("Number Of Maps", row.name, 'link_to', r.message.link)
                        ])
                      }
                      frm.refresh_fields();
                    })
                  }
                },
              });
              d.hide();
            },
          });

          d.show();
        } else {
          frappe.throw("Number Of Maps = 0");
        }
      });
      frm.add_custom_button(__("Scan Map"), () => {
        let maps = [];
        frm.doc.maps_name.forEach((element) => {
          maps.push(element.map);
        });
        const fields = [
          {
            fieldtype: "Link",
            fieldname: "map_name",
            options: "Map",
            label: __("Map"),
            reqd: 1,
            get_query: function (doc) {
              return { filters: { name: ["in", maps] } };
            },
          },
        ];
        var d = new frappe.ui.Dialog({
          title: __("Maps"),
          fields: fields,
          primary_action: function () {
            var data = d.get_values();
            frappe.run_serially([
              () => frappe.new_doc('Map Scanning'),
              () => {
                cur_frm.doc.map = data.map_name
                cur_frm.doc.from_map = 1
                cur_frm.set_df_property("map", "read_only", 1)
                cur_frm.refresh_fields()
              }
            ])
            d.hide();
          },
          primary_action_label: __("Scan Map"),
        });
        d.show();
      });
    }
    frm.refresh_fields();
  },

});

frappe.ui.form.on("Number Of Maps", {
  transfer_to_archive: function (frm, cdt, cdn) {
    let row = locals[cdt][cdn];
    let d = new frappe.ui.Dialog({
      title: "Transfer To Archive",
      fields: [
        {
          label: "Description",
          fieldname: "description",
          fieldtype: "SmallText",
          reqd: 1,
        },
        {
          label: "Attach File",
          fieldname: "attach_file",
          fieldtype: "Attach",
          reqd: 1,
        },
        {
          label: "Transfer Date",
          fieldname: "transfer_date",
          fieldtype: "Datetime",
          reqd: 1,
        }
      ],
      primary_action_label: "Transfer",
      primary_action(values) {
        frappe.call({
          method: "add_to_archive",
          doc: frm.doc,
          args: {
            description: values.description,
            transfer_date: values.transfer_date,
            attach_file: values.attach_file,
            map: row.map,
          },
          callback: (r) => {
            if (!r.exc) {
              row.map_status = "Archived"
              row.link_to = r.message.link
              frappe.run_serially([
                () => frappe.db.set_value("Number Of Maps", row.name, 'map_status', "Archived"),
                () => frappe.db.set_value("Number Of Maps", row.name, 'link_to', r.message.link)
              ])
              frappe.msgprint("Transferd To Archive Successfully !");
            }
            frm.refresh_fields();
          },
        });
        d.hide();
      },
    });

    d.show();
  }

})