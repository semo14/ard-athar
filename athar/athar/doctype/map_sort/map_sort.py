# -*- coding: utf-8 -*-
# Copyright (c) 2021, AnvilErp and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document


class MapSort(Document):
	@frappe.whitelist()
	def add_to_archive(self, description, transfer_date, attach_file, map,shelf_number):
		archive = frappe.new_doc('Archive Attachment')
		archive.description = description
		archive.transfer_date = transfer_date
		archive.attach_file = attach_file
		archive.map = map
		archive.shelf_number = shelf_number
		archive.map_sort = self.name
		archive.save()
		frappe.db.commit()
		return {'map':map, 'link':archive.name} 

	@frappe.whitelist()
	def get_maps(self):
		map_sort = frappe.get_doc('Map Sort', self.roll_number)
		mp = []
		for maps in map_sort.maps_name:
			mp.append(maps.map)
		return mp

	def on_cancel(self):
		self.db_set('roll_number',None)
		