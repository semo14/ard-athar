# -*- coding: utf-8 -*-
# Copyright (c) 2021, AnvilErp and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class MapRestoration(Document):
	def start_restoration(self):
		self.status = "Under Restoration"
		self.save()
	def end_restoration(self):
		self.status = "Restored & Transfered To Temporary Location Only"
		self.save()
	# def end_with_photo(self, photo):
	# 	if photo:
	# 		self.db_set('status', "Restored & Taken Photo & Transfered To Temporary Location", update_modified=False)
	# 	# self.save()