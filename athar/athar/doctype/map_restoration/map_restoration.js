// Copyright (c) 2021, AnvilErp and contributors
// For license information, please see license.txt

frappe.ui.form.on("Map Restoration", {
  refresh: function (frm) {
    cur_frm.set_df_property("map", "read_only", cur_frm.doc.from_map == 1);
    if (frm.doc.status == undefined && frm.doc.docstatus == 1) {
      cur_frm.remove_custom_button("End");
      frm.add_custom_button(__("Start"), () => {
        frappe.call({
          method: "start_restoration",
          doc: frm.doc,
          callback: function (r) {
            frm.reload_doc();
          },
        });
      });
    }
    if (frm.doc.status == "Under Restoration" && frm.doc.docstatus == 1) {
      cur_frm.remove_custom_button("Start");
      frm.add_custom_button(__("End"), () => {
        frappe.call({
          method: "end_restoration",
          doc: frm.doc,
          callback: function (r) {
            frm.reload_doc();
          },
        });
      });
    }

    if (
      frm.doc.status == "Restored & Transfered To Temporary Location Only" &&
      frm.doc.docstatus == 1
    ) {
      frm.toggle_display("add_photo", 1);
    } else {
      frm.toggle_display("add_photo", 0);
    }
  },
  add_photo: function (frm) {
    if (frm.doc.add_photo) {
      frm.doc.status =
        "Restored & Taken Photo & Transfered To Temporary Location";
      frm.refresh_field("status");
      frm.save();
      // frappe.call({
      // 	method: "end_with_photo",
      // 	args: {
      // 		photo: frm.doc.add_photo
      // 	},
      // 	doc: frm.doc,
      // 	callback: function (r) {
      // 	  frm.reload_doc();
      // 	},
      //   });
    }
  }
});
