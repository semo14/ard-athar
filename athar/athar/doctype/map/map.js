// Copyright (c) 2021, AnvilErp and contributors
// For license information, please see license.txt

frappe.ui.form.on('Map', {
	refresh: function(frm) {
		frm.set_query("map_area", function (doc) {
            if (frm.doc.map_group) {
                return {
                    query: "athar.queries.map_area_query",
                    filters: {
                        'map_group': frm.doc.map_group
                    }
                }
            }
            else {
                return { filters: {} }
            }
        });

		let name = frm.doc.name
		if(frm.doc.docstatus == 1){
			frm.add_custom_button(__('Scan Map'),
			()=> {
				frappe.run_serially([
					() => frappe.new_doc('Map Scanning'),
					() => frappe.timeout(1),
					() => {
						cur_frm.doc.from_map = 1
						cur_frm.set_df_property("map", "read_only", 1)
						cur_frm.doc.map = name;
						cur_frm.refresh_fields()
					}
				])
			})
			frm.add_custom_button(__('Create Map Evaluation'),
			()=> {
				frappe.run_serially([
					() => frappe.new_doc("Map Evaluation"),
					() => frappe.timeout(1),
					() => {
						cur_frm.doc.from_map = 1
						cur_frm.set_df_property("map", "read_only", 1)
						cur_frm.doc.map = name;
						cur_frm.refresh_fields()
					}
				])
			});
			frm.add_custom_button(__('Translate Map'),
			()=> {
				frappe.run_serially([
					() => frm.copy_doc(),
					() => frappe.timeout(1),
					() => {
						cur_frm.doc.translated = 1;
						cur_frm.doc.scaned = 0;
						cur_frm.doc.evaluated = 0;
						cur_frm.doc.translated_map = name;
						cur_frm.refresh_fields()
					} 
				])
				
			});
		frm.set_query("roll_number", function() {
			return{
				filters: { 'docstatus': 1 }
			}
		});
		}
	},
	map_group: function(frm){
		frm.set_query("map_area", function (doc) {
            if (frm.doc.map_group) {
                return {
                    query: "athar.queries.map_area_query",
                    filters: {
                        'map_group': frm.doc.map_group
                    }
                }
            }
            else {
                return { filters: {} }
            }
        });
	}
});
