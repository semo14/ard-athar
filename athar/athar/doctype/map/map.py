# -*- coding: utf-8 -*-
# Copyright (c) 2021, AnvilErp and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Map(Document):
	def on_submit(self):
		map_sort = frappe.get_doc('Map Sort', self.roll_number)

		if self.translated:
			if self.translated_map:
				map = frappe.get_doc('Map', self.translated_map)
				map.db_set('translated1', 1)
				
			for map_ in map_sort.maps_name:
				if map_.map == self.translated_map:
					map_sort.append('maps_name',{
						'map': self.name,
						'main_map': map_.map,
						'map_status': 'Map Translation'
					})
					map_sort.save()
					frappe.db.commit()
		else:
			current_number = int(map_sort.current_number) + 1
			map_sort.db_set('current_number', current_number)
			found = False
			for map_ in map_sort.maps_name:
				if map_.map == self.name:
					found=True
			if not found:
				map_sort.append('maps_name',{
					'map': self.name,
					'map_status': 'Saved'
				})
				map_sort.save()
				frappe.db.commit()


	def validate(self):
		if self.translated:
			return
		map_sort = frappe.get_doc('Map Sort', self.roll_number)
		if int(map_sort.current_number) >= map_sort.number_of_maps:
			frappe.throw('Roll Is Full')

	def after_insert(self):
		self.db_set('name' , f"{self.name}".upper())

	def on_cancel(self):
		map_sort = frappe.get_doc('Map Sort', self.roll_number)
		for map_ in map_sort.maps_name:
			if map_.map == self.name:
				map_ = frappe.get_doc("Number Of Maps", map_.name)
				map_.cancel()
				map_.delete()

				if self.translated: continue
				current_number = int(map_sort.current_number) - 1
				map_sort.db_set('current_number', current_number)
				frappe.db.commit()
