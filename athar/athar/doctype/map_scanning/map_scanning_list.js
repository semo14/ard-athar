frappe.listview_settings['Map Scanning'] = {
	get_indicator: function (doc) {
		if (doc.map_evaluation === "Scannable") {
			return [__("Scannable"), "green", "map_evaluation,=,Scannable"];
		} else if (doc.map_evaluation === "Unscannable") {
			return [__("Unscannable"), "blue", "map_evaluation,=,Unscannable"];
		} else if (doc.map_evaluation === "Need to Restoration") {
			return [__("Need to Restoration"), "yellow", "map_evaluation,=,Need to Restoration"];
		}
	}
};
