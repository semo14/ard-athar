// Copyright (c) 2021, AnvilErp and contributors
// For license information, please see license.txt

frappe.ui.form.on("Map Scanning", {
  refresh: function (frm) {
    cur_frm.set_df_property("map", "read_only", cur_frm.doc.from_map == 1);
    if (frm.doc.map_evaluation == "Scannable") {
		frm.toggle_display("add_map_photo", 1);
      } else if (frm.doc.map_evaluation == "Unscannable") {
		frm.toggle_display("add_map_photo", 1);
      } else if (frm.doc.map_evaluation == "Need to Restoration") {
		frm.toggle_display("add_map_photo", 0);
      }

    if (
      frm.doc.docstatus == 1 &&
      frm.doc.map_evaluation == "Need to Restoration"
    ) {
      frm.add_custom_button(__("Create Map Restoration"), () => {
        frappe.run_serially([
          () => frappe.new_doc("Map Restoration"),
          () => frappe.timeout(1),
          () => {
            cur_frm.doc.map = frm.doc.map;
            cur_frm.refresh_fields();
          },
        ]);
      });
    }
    frm.set_query("map", function() {
      return{
        filters: { 'docstatus': 1, 'scaned':0 }
      }
    });
  },
  map_evaluation: function (frm) {
    if (frm.doc.docstatus == 0) {
      if (frm.doc.map_evaluation == "Scannable") {
        frm.doc.status = "Scanning Is Done & Transfer To Temporary Place.";
		frm.toggle_display("add_map_photo", 1);
      } else if (frm.doc.map_evaluation == "Unscannable") {
        frm.doc.status = "Taken Photo & Transfer To Temporary Place.";
		frm.toggle_display("add_map_photo", 1);
      } else if (frm.doc.map_evaluation == "Need to Restoration") {
        frm.doc.status = "Need To Restoration";
		frm.toggle_display("add_map_photo", 0);
      }
      refresh_field("status");
    }
  },
});
