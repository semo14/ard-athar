# -*- coding: utf-8 -*-
# Copyright (c) 2021, AnvilErp and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class TransferMap(Document):

	def stop_transfer(self):
		if self.status == 'Pending':
			self.db_set('status','Stopped')
			self.save()
		elif self.status == 'Stopped':
			frappe.msgprint('Already Stopped')
		else:
			frappe.throw('You Can Not Stop Tranfer')

	def transfer_to_map_sort(self, map_sort):
		if self.status != 'Pending':
			frappe.throw('Transfer not allwed!')
		if self.docstatus != 1:
				frappe.throw('Submit Map Sort First')
		
		# New Map Sort
		map_sort = frappe.get_doc('Map Sort', map_sort)
		
		old_map_sort = frappe.get_doc('Map Sort', self.map_sort)
		map_ = frappe.get_doc('Map', self.map)
		

		for map_ in old_map_sort.maps_name:
			if self.map == map_.map:
				current_number = int(old_map_sort.current_number) - 1
				old_map_sort.db_set('current_number', current_number)

				number_of_maps = float(old_map_sort.number_of_maps) - 1
				old_map_sort.db_set('number_of_maps', number_of_maps)

				map_.db_set('parent', map_sort.name)
				map_.db_set('map_status', 'Saved')
				
				current_number = int(map_sort.current_number) + 1
				map_sort.db_set('current_number', current_number)

				number_of_map = float(map_sort.number_of_maps) + 1
				map_sort.db_set('number_of_maps', number_of_map)
				self.db_set('status', 'Transfer')
		
		for map_ in old_map_sort.maps_name:
			if self.map == map_.main_map:
				map_.db_set('parent', map_sort.name)

