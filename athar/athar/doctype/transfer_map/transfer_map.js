// Copyright (c) 2021, AnvilErp and contributors
// For license information, please see license.txt

frappe.ui.form.on("Transfer Map", {
  refresh: function (frm) {
    frm.set_query("archive", () => {
      return {
        filters: { docstatus: 1 },
      };
    });
    if (frm.doc.docstatus == 1) {
      frm.add_custom_button(__("Stop"), () => {
        frappe.call({
          method: "stop_transfer",
          doc: frm.doc,
          callback: function (r) {
            console.log(r.message);
          },
        });
        // if(frm.doc.status == 'Pending') {
        // 	frm.doc.status = 'Stopped'
        // 	frm.refresh_field('status')
        // }
      });
      frm.add_custom_button(__("Transfer"), () => {
        let d = new frappe.ui.Dialog({
          title: "Move To Another Map Sort",
          fields: [
            {
              label: "Map Sort",
              fieldname: "map_sort",
              fieldtype: "Link",
              options: "Map Sort",
              reqd: 1,
              get_query: function (doc) {
                return { filters: { docstatus: 1, name:['!=', frm.doc.map_sort] } };
              },
            },
          ],
          primary_action_label: "Transfer",
          primary_action(values) {
            frappe.call({
              method: "transfer_to_map_sort",
              doc: frm.doc,
              args: {
                map_sort: values.map_sort,
              },
              callback: function (r) {
                frappe.msgprint("Transferd Done");
              },
            });
            d.hide();
          },
        });
        d.show();
      });
    }
  },
});
