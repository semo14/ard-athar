
frappe.ui.form.on('Picture Scan', {
	refresh: function(frm) {
		if(frm.doc.docstatus == 1 && frm.doc.picture_evaluation == 'Scannable'){
			if(!frm.doc.hide_button){
				frm.add_custom_button(__('Create New Material'),
			()=>{
				frappe.run_serially([
					() => frappe.new_doc("Picture Material"),
					() => frappe.timeout(1),
					() => frm.doc.hide_button = 1,
					() => {
						cur_frm.doc.pictures = frm.doc.pictures;
						cur_frm.doc.picture_scan = frm.doc.name;
						cur_frm.refresh_fields();
					},
				]);
			});
			frm.add_custom_button(__('Add As Copy'),
			()=>{
				frappe.run_serially([
					() => frappe.new_doc("Picture Material"),
					() => frappe.timeout(1),
					() => frm.doc.hide_button = 1,
					() => {
						cur_frm.doc.pictures = frm.doc.pictures;
						cur_frm.doc.copy = 1;
						cur_frm.refresh_fields();
					},
				]);
			});
			frm.add_custom_button(__('Link Picture With Material'),
			()=>{
				frappe.run_serially([
					() => frappe.new_doc("Picture Material"),
					() => frappe.timeout(1),
					() => frm.doc.hide_button = 1,
					() => {
						cur_frm.doc.pictures = frm.doc.pictures;
						cur_frm.doc.linked = 1
						cur_frm.refresh_fields();
					},
				]);
			})
			}
		}
	},
	picture_evaluation:(frm)=>{
		if(frm.doc.picture_evaluation == 'Unscannable'){
		frm.toggle_display('attach_image',1)
		frm.refresh_field('attach_image')
		frm.toggle_display(['old_code_material', 'envelope_code'],0)
		frm.toggle_reqd(['envelope_code','old_code_material'],0)
		// frm.set_df_property("picture_evaluation", "read_only", 1)
	}
	else if(frm.doc.picture_evaluation == 'Scannable'){
		frm.toggle_display(['old_code_material', 'envelope_code'],1)
		frm.toggle_reqd(['envelope_code','old_code_material'],1)
		frm.toggle_display('attach_image',0)
		// frm.set_df_property("picture_evaluation", "read_only", 1)	
	};
}
});