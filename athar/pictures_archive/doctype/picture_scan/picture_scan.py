from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class PictureScan(Document):
	def on_submit(self):
		if self.picture_evaluation == 'Unscannable':
			self.status = 'Unscannable And Taken Picture'

		old_code = self.old_code_material
		if frappe.db.exists('Old Code Material', old_code):
			pic_scan_list = frappe.get_list('Picture Scan', fields=['name', 'old_code_material', 'pictures', 'material_type'], filters={ 'name':['!=', self.name] ,'old_code_material':old_code,'docstatus':1})
			for pic in pic_scan_list:
				material_type = frappe.get_doc('Material Type', pic.material_type)
				frappe.msgprint(f'There Is An Item With The Entered Code <b>{self.old_code_material}</b>, <b>Material <a href="#Form/Pictures/{pic.pictures}"> {material_type.material_name}</a></b>, From An <b>Item  <a href="#Form/Picture%20Scan/{pic.name}">{pic.name}</a></b>')
		
	def on_cancel(self):
		self.flags.ignore_links = True
