from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class PictureMaterial(Document):
	def on_submit(self):
		pictures = frappe.get_doc('Pictures', self.pictures)
		if self.translated:
			pic_material = frappe.get_doc('Picture Material', self.translated_material_type)
			pic_material.db_set('material_translation' ,1)
			# Append Material Type In Pictures Doctype
			pictures.append('all_pictures_material',{
				'material_type': self.name,
				'status': 'Translated',
				'material_translated': self.translated_material_type,})
		elif self.copy:
			pictures.append('all_pictures_material',{
				'material_type': self.name,
				'status': 'Copy',})

		elif self.linked:
			pictures.append('all_pictures_material',{
				'material_type': self.name,
				'status': 'Linked',})

		else:
			pictures.append('all_pictures_material',{
				'material_type': self.name,
				'status': 'New Material',
			})


		pictures.save()
		frappe.db.commit()

	def after_insert(self):
		self.db_set('name' , f"{self.name}".upper())

	def on_cancel(self):
		self.flags.ignore_links = True
		# Get Pictures Doctype  To Increase Container Element When Cancel MAterial Type
		pictures = frappe.get_doc('Pictures', self.pictures)		
		# Remove Picture MAterial From Tabel In Pictures
		for material in pictures.all_pictures_material:
			if material.material_type == self.name or material.material_translated == self.name:
				material_type = frappe.get_doc('All Pictures Material', material.name)
				if material_type.status != 'Translated':
					#increase Container Elements Number +1
					container_element = pictures.container_element + 1
					pictures.db_set('container_element', container_element)

				material_type.cancel()
				material_type.delete()
