frappe.ui.form.on('Picture Material', {
	refresh: function(frm) {
		if(frm.doc.docstatus == 1){
			let docname = frm.doc.name
			frm.add_custom_button(__('Translate'), ()=>{
				frappe.run_serially([
					() => frm.copy_doc(),
					() => frm.doc.material_translation = 1,
					() => frappe.timeout(1),
					() => {
						cur_frm.doc.translated = 1;
						// cur_frm.doc.scaned = 0;
						// cur_frm.doc.evaluated = 0;
						cur_frm.doc.pictures = frm.doc.pictures;
						cur_frm.doc.translated_material_type = docname
						cur_frm.refresh_fields()
					} 
				])
			});
			frm.add_custom_button(__('Create Material Evaluation'),
			()=> {
				frappe.run_serially([
					() => frappe.new_doc("Picture Material Evaluation"),
					() => frm.doc.evaluated = 1,
					() => frappe.timeout(1),
					() => {
						cur_frm.doc.picture_material = docname;
						cur_frm.refresh_fields()
					}
				])
			});
			
		}
	}
});