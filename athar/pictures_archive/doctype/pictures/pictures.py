# -*- coding: utf-8 -*-
# Copyright (c) 2021, AnvilErp and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Pictures(Document):
	def validate(self):
		count =0
		for material in self.all_pictures_material:
			if material.status != 'Translated':
				count +=1
		if self.container_element == count:
			frappe.msgprint("Can't Scan Another Picture")

	def on_cancel(self):
		self.flags.ignore_links = True
	
	def on_trash(self):
		self.flags.ignore_links = True
