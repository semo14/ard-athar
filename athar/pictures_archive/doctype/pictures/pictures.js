frappe.ui.form.on('Pictures', {
	refresh: function(frm) {
		frm.trigger('count')
		// Add Scan Btn
		if(frm.doc.docstatus == 1){
			let count = 0
			frm.doc.all_pictures_material.forEach(pic => {
				if(pic.status != 'Translated'){
					count +=1
				}
			})
			frm.add_custom_button(__('Scan Picture'), ()=>{
				if(count < frm.doc.container_element){
					frappe.run_serially([
						() => frappe.new_doc('Picture Scan'),
						() => frappe.timeout(1),
						() => {
							cur_frm.doc.pictures = frm.doc.name
							cur_frm.doc.material_type = frm.doc.material_type
							cur_frm.refresh_fields()
						}
					])
				}
				else{
					frappe.msgprint("Can't Scan Another Picture")
				}
					
			})
		}
	},
	count:(frm)=>{
		if(frm.doc.count == 'Group'){
			frm.toggle_display(['container_name', 'container_code', 'container_element'],1)
			frm.toggle_reqd(['container_name', 'container_code', 'container_element'],1)
			frm.toggle_display(['image_name', 'image_code'],0)
			frm.toggle_reqd(['image_name', 'image_code'],0)
		}else if(frm.doc.count == 'Single Piece'){
			frm.toggle_display(['image_name', 'image_code'],1)
			frm.set_df_property(['image_name', 'image_code'],'reqd',1)
			frm.toggle_reqd(['image_name', 'image_code'],1)
			frm.toggle_display(['container_name', 'container_code', 'container_element'],0)
			frm.toggle_reqd(['container_name', 'container_code', 'container_element'],0)
		};
	}
});