
from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class PictureMaterialEvaluation(Document):
	def on_submit(self):
		pic_material = frappe.get_doc('Picture Material', self.picture_material)
		pic_material.db_set('evaluated', 1, update_modified=False)
		frappe.db.commit()