from __future__ import unicode_literals


import frappe
# import erpnext

from frappe.desk.reportview import get_match_cond, get_filters_cond
from collections import defaultdict

@frappe.whitelist()
# @frappe.validate_and_sanitize_search_inputs
def map_area_query(doctype, txt, searchfield, start, page_len, filters):
    map_group = filters.get('map_group', False)
    conds = ""
    conditions = []
    filter_dict = {}
    if map_group:
        conds = " and `tabMap Area Childtable`.parent='{}' ".format(map_group)
    
        query = """select `tabMap Area`.name, `tabMap Area`.area_name, `tabMap Area`.area_code 
            from `tabMap Area Childtable` 
            left join `tabMap Area` ON `tabMap Area Childtable`.area=`tabMap Area`.name 
            where
            (`tabMap Area`.`{key}` LIKE {txt} OR `tabMap Area`.`{key2}` LIKE {txt} 
                OR `tabMap Area`.`{key3}` LIKE {txt} 
                {fcond} {mcond} ) {conds}
            order by
                `tabMap Area`.name desc
            limit
                {start}, {page_len}
            """.format(
                key=searchfield,
                key2='area_name',
                key3='area_code',
                conds=conds,
                fcond=get_filters_cond(doctype, filter_dict.get("Map Area"), conditions),
                mcond=get_match_cond(doctype),
                start=start,
                page_len=page_len,
                txt=frappe.db.escape('%{0}%'.format(txt))
            )
    else:
        return ()
    return frappe.db.sql(query)


def get_doctype_wise_filters(filters):
    # Helper function to seperate filters doctype_wise
    filter_dict = defaultdict(list)
    for row in filters:
        filter_dict[row[0]].append(row)
    return filter_dict
