# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in athar/__init__.py
from athar import __version__ as version

setup(
	name='athar',
	version=version,
	description=' ',
	author='AnvilErp',
	author_email='anvilerp@support.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
